import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class FilterFuctionService {

    constructor() { }

    filterFunc(item, keyword: string) {

        let cek = false;
        Object.keys(item).map(function (key) {
            if (item[key]) {
                if (!isNaN(item[key])) {
                    if(item[key].toString().indexOf(keyword) !== -1 ) { cek = true}
                }
                else {
                    if(item[key].toLowerCase().indexOf(keyword.toLowerCase()) !== -1 ) { cek = true}
                }

            }

        });
        return cek;

    };


}
