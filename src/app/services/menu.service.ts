import { Injectable } from '@angular/core'
import { Observable, of, BehaviorSubject } from 'rxjs'

@Injectable({
    providedIn: 'root',
})
export class MenuService {
    constructor() {}

    menu = new BehaviorSubject<any>([])
    getMenuData(): Observable<any[]> {
        return this.menu.asObservable()
    }
}
