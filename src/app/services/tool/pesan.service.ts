import { Injectable } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd';

@Injectable({
    providedIn: 'root'
})
export class PesanService {

    constructor(private message: NzMessageService) { }

    pesanWarningForm(type: string): void {
        this.message.create('warning', type);
    }

}
