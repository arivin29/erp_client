/* tslint:disable */
export interface Perizinan {

  /**
   * tentang_izin
   */
  tentang_izin?: string;

  /**
   * id_perizinan
   */
  id_perizinan?: number;

  /**
   * tanggal_berlaku
   */
  tanggal_berlaku?: string;

  /**
   * tanggal_berakhir
   */
  tanggal_berakhir?: string;

  /**
   * status_izin
   */
  status_izin?: string;

  /**
   * id_perusahaan
   */
  id_perusahaan?: number;

  /**
   * nomor_izin
   */
  nomor_izin?: string;

  /**
   * isi array json, karena 1 izin bisa banyak file
   */
  id_dokumen?: string;

  /**
   * id_user_create
   */
  id_user_create?: number;

  /**
   * created_at
   */
  created_at?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
