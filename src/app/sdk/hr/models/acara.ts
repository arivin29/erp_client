/* tslint:disable */
export interface Acara {

  /**
   * id_acara
   */
  id_acara?: number;

  /**
   * judul_acara
   */
  judul_acara?: string;

  /**
   * tanggal_mulai
   */
  tanggal_mulai?: string;

  /**
   * tanggal_selesai
   */
  tanggal_selesai?: string;

  /**
   * untuk puplic atau privat
   */
  peruntukan?: string;

  /**
   * id_dokumen
   */
  id_dokumen?: number;

  /**
   * id_user_create
   */
  id_user_create?: number;

  /**
   * created_at
   */
  created_at?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
