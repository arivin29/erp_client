/* tslint:disable */
export interface Divisi {
    /**
     * id_divisi
     */
    id_divisi?: string

    /**
     * id_departemen
     */
    id_departemen?: string

    /**
     * nama_divisi
     */
    nama_divisi?: string

    /**
     * created_at
     */
    created_at?: string

    /**
     * updated_at
     */
    updated_at?: string
}
