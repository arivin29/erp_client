/* tslint:disable */
export interface Lisensi {

  /**
   * tentang_lisensi
   */
  tentang_lisensi?: string;

  /**
   * id_lisensi
   */
  id_lisensi?: number;

  /**
   * tanggal_berlaku
   */
  tanggal_berlaku?: string;

  /**
   * tanggal_berakhir
   */
  tanggal_berakhir?: string;

  /**
   * status_lisensi
   */
  status_lisensi?: string;

  /**
   * id_perusahaan
   */
  id_perusahaan?: number;

  /**
   * nomor_lisensi
   */
  nomor_lisensi?: string;

  /**
   * id_dokumen
   */
  id_dokumen?: number;

  /**
   * id_user_create
   */
  id_user_create?: number;

  /**
   * created_at
   */
  created_at?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
