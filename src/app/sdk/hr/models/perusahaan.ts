/* tslint:disable */
export interface Perusahaan {

  /**
   * email
   */
  email?: string;

  /**
   * id_perusahaan
   */
  id_perusahaan?: string;

  /**
   * pic
   */
  pic?: string;

  /**
   * alamat
   */
  alamat?: string;

  /**
   * negara
   */
  negara?: string;

  /**
   * propinsi
   */
  propinsi?: string;

  /**
   * kota
   */
  kota?: string;

  /**
   * kode_pos
   */
  kode_pos?: string;

  /**
   * nama
   */
  nama?: string;

  /**
   * hp
   */
  hp?: string;

  /**
   * telfon
   */
  telfon?: string;

  /**
   * fax
   */
  fax?: string;

  /**
   * website
   */
  website?: string;

  /**
   * logo
   */
  logo?: string;

  /**
   * id_user_create
   */
  id_user_create?: number;

  /**
   * created_at
   */
  created_at?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
