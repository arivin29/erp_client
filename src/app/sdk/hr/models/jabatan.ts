/* tslint:disable */
export interface Jabatan {

  /**
   * id_jabatan
   */
  id_jabatan?: string;

  /**
   * id_parent_jabatan
   */
  id_parent_jabatan?: string;

  /**
   * id_divisi
   */
  id_divisi?: string;

  /**
   * nama_jabatan
   */
  nama_jabatan?: string;

  /**
   * id_departemen
   */
  id_departemen?: string;

  /**
   * created_at
   */
  created_at?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
