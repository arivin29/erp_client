/* tslint:disable */
export interface Dokumen {

  /**
   * lokasi_file
   */
  lokasi_file?: string;

  /**
   * id_dokumen
   */
  id_dokumen?: number;

  /**
   * nomor
   */
  nomor?: string;

  /**
   * tanggal_dokumen
   */
  tanggal_dokumen?: string;

  /**
   * for_module
   */
  for_module?: string;

  /**
   * for_module_id
   */
  for_module_id?: number;

  /**
   * nama_dokumen
   */
  nama_dokumen?: string;

  /**
   * nama_file
   */
  nama_file?: string;

  /**
   * ukuran_file
   */
  ukuran_file?: string;

  /**
   * tanggal_upload
   */
  tanggal_upload?: string;

  /**
   * id_user_create
   */
  id_user_create?: number;

  /**
   * created_at
   */
  created_at?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
