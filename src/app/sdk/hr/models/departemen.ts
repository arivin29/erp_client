/* tslint:disable */
export interface Departemen {

  /**
   * id_departemen
   */
  id_departemen?: string;

  /**
   * nama_departemen
   */
  nama_departemen?: string;

  /**
   * deskripsi_departement
   */
  deskripsi_departement?: string;

  /**
   * created_at
   */
  created_at?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
