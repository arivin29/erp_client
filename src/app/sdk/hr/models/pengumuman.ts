/* tslint:disable */
export interface Pengumuman {

  /**
   * di left join
   */
  id_departement?: number;

  /**
   * id_pengumuman
   */
  id_pengumuman?: number;

  /**
   * mulai_berlaku
   */
  mulai_berlaku?: string;

  /**
   * berakhir_sampai
   */
  berakhir_sampai?: string;

  /**
   * di left join
   */
  id_kantor?: number;

  /**
   * di left join
   */
  id_divisi?: number;

  /**
   * judul
   */
  judul?: string;

  /**
   * id_pegawai
   */
  id_pegawai?: number;

  /**
   * id_dokumen
   */
  id_dokumen?: number;

  /**
   * id_user_create
   */
  id_user_create?: number;

  /**
   * created_at
   */
  created_at?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
