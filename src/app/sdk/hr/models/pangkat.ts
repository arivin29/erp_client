/* tslint:disable */
export interface Pangkat {

  /**
   * id_pangkat
   */
  id_pangkat?: string;

  /**
   * nama_pangkat
   */
  nama_pangkat?: string;

  /**
   * urutan_pangkat
   */
  urutan_pangkat?: number;

  /**
   * level_pangkat
   */
  level_pangkat?: string;

  /**
   * created_at
   */
  created_at?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
