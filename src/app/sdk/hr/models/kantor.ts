/* tslint:disable */
export interface Kantor {

  /**
   * id_kantor
   */
  id_kantor?: string;

  /**
   * id_perusahaan
   */
  id_perusahaan?: string;

  /**
   * nama_kantor
   */
  nama_kantor?: string;

  /**
   * kode_kantor
   */
  kode_kantor?: string;

  /**
   * pic_kantor
   */
  pic_kantor?: string;

  /**
   * phone_kantor
   */
  phone_kantor?: string;

  /**
   * alamat_kantor
   */
  alamat_kantor?: string;

  /**
   * aktif, nonaktif
   */
  status_kantor?: string;

  /**
   * created_at
   */
  created_at?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
