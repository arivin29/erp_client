/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Perusahaan } from '../models/perusahaan';
@Injectable({
  providedIn: 'root',
})
class CompanyPerusahaanService extends __BaseService {
  static readonly getCompanyPerusahaansPath = '/company/perusahaans';
  static readonly postCompanyPerusahaansPath = '/company/perusahaans';
  static readonly getCompanyPerusahaansIdPath = '/company/perusahaans/{id}';
  static readonly putCompanyPerusahaansIdPath = '/company/perusahaans/{id}';
  static readonly deleteCompanyPerusahaansIdPath = '/company/perusahaans/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get all Perusahaans
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCompanyPerusahaansResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Perusahaan>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/company/perusahaans`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Perusahaan>, message?: string}>;
      })
    );
  }
  /**
   * Get all Perusahaans
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCompanyPerusahaans(filter?: string): __Observable<{success?: boolean, data?: Array<Perusahaan>, message?: string}> {
    return this.getCompanyPerusahaansResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Perusahaan>, message?: string})
    );
  }

  /**
   * Store Perusahaan
   * @param body Perusahaan that should be stored
   * @return successful operation
   */
  postCompanyPerusahaansResponse(body?: Perusahaan): __Observable<__StrictHttpResponse<{success?: boolean, data?: Perusahaan, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/company/perusahaans`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Perusahaan, message?: string}>;
      })
    );
  }
  /**
   * Store Perusahaan
   * @param body Perusahaan that should be stored
   * @return successful operation
   */
  postCompanyPerusahaans(body?: Perusahaan): __Observable<{success?: boolean, data?: Perusahaan, message?: string}> {
    return this.postCompanyPerusahaansResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Perusahaan, message?: string})
    );
  }

  /**
   * Get Perusahaan
   * @param id id of Perusahaan
   * @return successful operation
   */
  getCompanyPerusahaansIdResponse(id: number): __Observable<__StrictHttpResponse<{success?: boolean, data?: Perusahaan, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/company/perusahaans/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Perusahaan, message?: string}>;
      })
    );
  }
  /**
   * Get Perusahaan
   * @param id id of Perusahaan
   * @return successful operation
   */
  getCompanyPerusahaansId(id: number): __Observable<{success?: boolean, data?: Perusahaan, message?: string}> {
    return this.getCompanyPerusahaansIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Perusahaan, message?: string})
    );
  }

  /**
   * Update Perusahaan
   * @param params The `CompanyPerusahaanService.PutCompanyPerusahaansIdParams` containing the following parameters:
   *
   * - `id`: id of Perusahaan
   *
   * - `body`: Perusahaan that should be updated
   *
   * @return successful operation
   */
  putCompanyPerusahaansIdResponse(params: CompanyPerusahaanService.PutCompanyPerusahaansIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Perusahaan, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/company/perusahaans/${params.id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Perusahaan, message?: string}>;
      })
    );
  }
  /**
   * Update Perusahaan
   * @param params The `CompanyPerusahaanService.PutCompanyPerusahaansIdParams` containing the following parameters:
   *
   * - `id`: id of Perusahaan
   *
   * - `body`: Perusahaan that should be updated
   *
   * @return successful operation
   */
  putCompanyPerusahaansId(params: CompanyPerusahaanService.PutCompanyPerusahaansIdParams): __Observable<{success?: boolean, data?: Perusahaan, message?: string}> {
    return this.putCompanyPerusahaansIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Perusahaan, message?: string})
    );
  }

  /**
   * Delete Perusahaan
   * @param id id of Perusahaan
   * @return successful operation
   */
  deleteCompanyPerusahaansIdResponse(id: number): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/company/perusahaans/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Delete Perusahaan
   * @param id id of Perusahaan
   * @return successful operation
   */
  deleteCompanyPerusahaansId(id: number): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCompanyPerusahaansIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CompanyPerusahaanService {

  /**
   * Parameters for putCompanyPerusahaansId
   */
  export interface PutCompanyPerusahaansIdParams {

    /**
     * id of Perusahaan
     */
    id: number;

    /**
     * Perusahaan that should be updated
     */
    body?: Perusahaan;
  }
}

export { CompanyPerusahaanService }
