/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Kantor } from '../models/kantor';
@Injectable({
  providedIn: 'root',
})
class CompanyKantorService extends __BaseService {
  static readonly getCompanyKantorsPath = '/company/kantors';
  static readonly postCompanyKantorsPath = '/company/kantors';
  static readonly getCompanyKantorsIdPath = '/company/kantors/{id}';
  static readonly putCompanyKantorsIdPath = '/company/kantors/{id}';
  static readonly deleteCompanyKantorsIdPath = '/company/kantors/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get all Kantors
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCompanyKantorsResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Kantor>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/company/kantors`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Kantor>, message?: string}>;
      })
    );
  }
  /**
   * Get all Kantors
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCompanyKantors(filter?: string): __Observable<{success?: boolean, data?: Array<Kantor>, message?: string}> {
    return this.getCompanyKantorsResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Kantor>, message?: string})
    );
  }

  /**
   * Store Kantor
   * @param body Kantor that should be stored
   * @return successful operation
   */
  postCompanyKantorsResponse(body?: Kantor): __Observable<__StrictHttpResponse<{success?: boolean, data?: Kantor, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/company/kantors`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Kantor, message?: string}>;
      })
    );
  }
  /**
   * Store Kantor
   * @param body Kantor that should be stored
   * @return successful operation
   */
  postCompanyKantors(body?: Kantor): __Observable<{success?: boolean, data?: Kantor, message?: string}> {
    return this.postCompanyKantorsResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Kantor, message?: string})
    );
  }

  /**
   * Get Kantor
   * @param id id of Kantor
   * @return successful operation
   */
  getCompanyKantorsIdResponse(id: number): __Observable<__StrictHttpResponse<{success?: boolean, data?: Kantor, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/company/kantors/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Kantor, message?: string}>;
      })
    );
  }
  /**
   * Get Kantor
   * @param id id of Kantor
   * @return successful operation
   */
  getCompanyKantorsId(id: number): __Observable<{success?: boolean, data?: Kantor, message?: string}> {
    return this.getCompanyKantorsIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Kantor, message?: string})
    );
  }

  /**
   * Update Kantor
   * @param params The `CompanyKantorService.PutCompanyKantorsIdParams` containing the following parameters:
   *
   * - `id`: id of Kantor
   *
   * - `body`: Kantor that should be updated
   *
   * @return successful operation
   */
  putCompanyKantorsIdResponse(params: CompanyKantorService.PutCompanyKantorsIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Kantor, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/company/kantors/${params.id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Kantor, message?: string}>;
      })
    );
  }
  /**
   * Update Kantor
   * @param params The `CompanyKantorService.PutCompanyKantorsIdParams` containing the following parameters:
   *
   * - `id`: id of Kantor
   *
   * - `body`: Kantor that should be updated
   *
   * @return successful operation
   */
  putCompanyKantorsId(params: CompanyKantorService.PutCompanyKantorsIdParams): __Observable<{success?: boolean, data?: Kantor, message?: string}> {
    return this.putCompanyKantorsIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Kantor, message?: string})
    );
  }

  /**
   * Delete Kantor
   * @param id id of Kantor
   * @return successful operation
   */
  deleteCompanyKantorsIdResponse(id: number): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/company/kantors/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Delete Kantor
   * @param id id of Kantor
   * @return successful operation
   */
  deleteCompanyKantorsId(id: number): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCompanyKantorsIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CompanyKantorService {

  /**
   * Parameters for putCompanyKantorsId
   */
  export interface PutCompanyKantorsIdParams {

    /**
     * id of Kantor
     */
    id: number;

    /**
     * Kantor that should be updated
     */
    body?: Kantor;
  }
}

export { CompanyKantorService }
