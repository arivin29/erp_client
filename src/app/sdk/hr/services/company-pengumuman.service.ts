/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Pengumuman } from '../models/pengumuman';
@Injectable({
  providedIn: 'root',
})
class CompanyPengumumanService extends __BaseService {
  static readonly getCompanyPengumumenPath = '/company/pengumumen';
  static readonly postCompanyPengumumenPath = '/company/pengumumen';
  static readonly getCompanyPengumumenIdPath = '/company/pengumumen/{id}';
  static readonly putCompanyPengumumenIdPath = '/company/pengumumen/{id}';
  static readonly deleteCompanyPengumumenIdPath = '/company/pengumumen/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get all Pengumumen
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCompanyPengumumenResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Pengumuman>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/company/pengumumen`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Pengumuman>, message?: string}>;
      })
    );
  }
  /**
   * Get all Pengumumen
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCompanyPengumumen(filter?: string): __Observable<{success?: boolean, data?: Array<Pengumuman>, message?: string}> {
    return this.getCompanyPengumumenResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Pengumuman>, message?: string})
    );
  }

  /**
   * Store Pengumuman
   * @param body Pengumuman that should be stored
   * @return successful operation
   */
  postCompanyPengumumenResponse(body?: Pengumuman): __Observable<__StrictHttpResponse<{success?: boolean, data?: Pengumuman, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/company/pengumumen`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Pengumuman, message?: string}>;
      })
    );
  }
  /**
   * Store Pengumuman
   * @param body Pengumuman that should be stored
   * @return successful operation
   */
  postCompanyPengumumen(body?: Pengumuman): __Observable<{success?: boolean, data?: Pengumuman, message?: string}> {
    return this.postCompanyPengumumenResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Pengumuman, message?: string})
    );
  }

  /**
   * Get Pengumuman
   * @param id id of Pengumuman
   * @return successful operation
   */
  getCompanyPengumumenIdResponse(id: number): __Observable<__StrictHttpResponse<{success?: boolean, data?: Pengumuman, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/company/pengumumen/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Pengumuman, message?: string}>;
      })
    );
  }
  /**
   * Get Pengumuman
   * @param id id of Pengumuman
   * @return successful operation
   */
  getCompanyPengumumenId(id: number): __Observable<{success?: boolean, data?: Pengumuman, message?: string}> {
    return this.getCompanyPengumumenIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Pengumuman, message?: string})
    );
  }

  /**
   * Update Pengumuman
   * @param params The `CompanyPengumumanService.PutCompanyPengumumenIdParams` containing the following parameters:
   *
   * - `id`: id of Pengumuman
   *
   * - `body`: Pengumuman that should be updated
   *
   * @return successful operation
   */
  putCompanyPengumumenIdResponse(params: CompanyPengumumanService.PutCompanyPengumumenIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Pengumuman, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/company/pengumumen/${params.id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Pengumuman, message?: string}>;
      })
    );
  }
  /**
   * Update Pengumuman
   * @param params The `CompanyPengumumanService.PutCompanyPengumumenIdParams` containing the following parameters:
   *
   * - `id`: id of Pengumuman
   *
   * - `body`: Pengumuman that should be updated
   *
   * @return successful operation
   */
  putCompanyPengumumenId(params: CompanyPengumumanService.PutCompanyPengumumenIdParams): __Observable<{success?: boolean, data?: Pengumuman, message?: string}> {
    return this.putCompanyPengumumenIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Pengumuman, message?: string})
    );
  }

  /**
   * Delete Pengumuman
   * @param id id of Pengumuman
   * @return successful operation
   */
  deleteCompanyPengumumenIdResponse(id: number): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/company/pengumumen/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Delete Pengumuman
   * @param id id of Pengumuman
   * @return successful operation
   */
  deleteCompanyPengumumenId(id: number): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCompanyPengumumenIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CompanyPengumumanService {

  /**
   * Parameters for putCompanyPengumumenId
   */
  export interface PutCompanyPengumumenIdParams {

    /**
     * id of Pengumuman
     */
    id: number;

    /**
     * Pengumuman that should be updated
     */
    body?: Pengumuman;
  }
}

export { CompanyPengumumanService }
