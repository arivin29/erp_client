/* tslint:disable */
import { Injectable } from '@angular/core'
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http'
import { BaseService as __BaseService } from '../base-service'
import { ApiConfiguration as __Configuration } from '../api-configuration'
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response'
import { Observable as __Observable } from 'rxjs'
import { map as __map, filter as __filter } from 'rxjs/operators'

import { Divisi } from '../models/divisi'
@Injectable({
    providedIn: 'root',
})
class CompanyDivisiService extends __BaseService {
    static readonly getCompanyDivisisPath = '/company/divisis'
    static readonly postCompanyDivisisPath = '/company/divisis'
    static readonly getCompanyDivisisIdPath = '/company/divisis/{id}'
    static readonly putCompanyDivisisIdPath = '/company/divisis/{id}'
    static readonly deleteCompanyDivisisIdPath = '/company/divisis/{id}'

    constructor(config: __Configuration, http: HttpClient) {
        super(config, http)
    }

    /**
     * Get all Divisis
     * @param filter PurchaseRequest that should be stored
     * @return successful operation
     */
    getCompanyDivisisResponse(
        filter?: string,
    ): __Observable<
        __StrictHttpResponse<{ success?: boolean; data?: Array<Divisi>; message?: string }>
    > {
        let __params = this.newParams()
        let __headers = new HttpHeaders()
        let __body: any = null
        if (filter != null) __params = __params.set('filter', filter.toString())
        let req = new HttpRequest<any>('GET', this.rootUrl + `/company/divisis`, __body, {
            headers: __headers,
            params: __params,
            responseType: 'json',
        })

        return this.http.request<any>(req).pipe(
            __filter(_r => _r instanceof HttpResponse),
            __map(_r => {
                return _r as __StrictHttpResponse<{
                    success?: boolean
                    data?: Array<Divisi>
                    message?: string
                }>
            }),
        )
    }
    /**
     * Get all Divisis
     * @param filter PurchaseRequest that should be stored
     * @return successful operation
     */
    getCompanyDivisis(
        filter?: string,
    ): __Observable<{ success?: boolean; data?: Array<Divisi>; message?: string }> {
        return this.getCompanyDivisisResponse(filter).pipe(
            __map(_r => _r.body as { success?: boolean; data?: Array<Divisi>; message?: string }),
        )
    }

    /**
     * Store Divisi
     * @param body Divisi that should be stored
     * @return successful operation
     */
    postCompanyDivisisResponse(
        body?: Divisi,
    ): __Observable<__StrictHttpResponse<{ success?: boolean; data?: Divisi; message?: string }>> {
        let __params = this.newParams()
        let __headers = new HttpHeaders()
        let __body: any = null
        __body = body
        let req = new HttpRequest<any>('POST', this.rootUrl + `/company/divisis`, __body, {
            headers: __headers,
            params: __params,
            responseType: 'json',
        })

        return this.http.request<any>(req).pipe(
            __filter(_r => _r instanceof HttpResponse),
            __map(_r => {
                return _r as __StrictHttpResponse<{
                    success?: boolean
                    data?: Divisi
                    message?: string
                }>
            }),
        )
    }
    /**
     * Store Divisi
     * @param body Divisi that should be stored
     * @return successful operation
     */
    postCompanyDivisis(
        body?: Divisi,
    ): __Observable<{ success?: boolean; data?: Divisi; message?: string }> {
        return this.postCompanyDivisisResponse(body).pipe(
            __map(_r => _r.body as { success?: boolean; data?: Divisi; message?: string }),
        )
    }

    /**
     * Get Divisi
     * @param id id of Divisi
     * @return successful operation
     */
    getCompanyDivisisIdResponse(
        id: string,
    ): __Observable<__StrictHttpResponse<{ success?: boolean; data?: Divisi; message?: string }>> {
        let __params = this.newParams()
        let __headers = new HttpHeaders()
        let __body: any = null

        let req = new HttpRequest<any>('GET', this.rootUrl + `/company/divisis/${id}`, __body, {
            headers: __headers,
            params: __params,
            responseType: 'json',
        })

        return this.http.request<any>(req).pipe(
            __filter(_r => _r instanceof HttpResponse),
            __map(_r => {
                return _r as __StrictHttpResponse<{
                    success?: boolean
                    data?: Divisi
                    message?: string
                }>
            }),
        )
    }
    /**
     * Get Divisi
     * @param id id of Divisi
     * @return successful operation
     */
    getCompanyDivisisId(
        id: string,
    ): __Observable<{ success?: boolean; data?: Divisi; message?: string }> {
        return this.getCompanyDivisisIdResponse(id).pipe(
            __map(_r => _r.body as { success?: boolean; data?: Divisi; message?: string }),
        )
    }

    /**
     * Update Divisi
     * @param params The `CompanyDivisiService.PutCompanyDivisisIdParams` containing the following parameters:
     *
     * - `id`: id of Divisi
     *
     * - `body`: Divisi that should be updated
     *
     * @return successful operation
     */
    putCompanyDivisisIdResponse(
        params: CompanyDivisiService.PutCompanyDivisisIdParams,
    ): __Observable<__StrictHttpResponse<{ success?: boolean; data?: Divisi; message?: string }>> {
        let __params = this.newParams()
        let __headers = new HttpHeaders()
        let __body: any = null

        __body = params.body
        let req = new HttpRequest<any>(
            'PUT',
            this.rootUrl + `/company/divisis/${params.id}`,
            __body,
            {
                headers: __headers,
                params: __params,
                responseType: 'json',
            },
        )

        return this.http.request<any>(req).pipe(
            __filter(_r => _r instanceof HttpResponse),
            __map(_r => {
                return _r as __StrictHttpResponse<{
                    success?: boolean
                    data?: Divisi
                    message?: string
                }>
            }),
        )
    }
    /**
     * Update Divisi
     * @param params The `CompanyDivisiService.PutCompanyDivisisIdParams` containing the following parameters:
     *
     * - `id`: id of Divisi
     *
     * - `body`: Divisi that should be updated
     *
     * @return successful operation
     */
    putCompanyDivisisId(
        params: CompanyDivisiService.PutCompanyDivisisIdParams,
    ): __Observable<{ success?: boolean; data?: Divisi; message?: string }> {
        return this.putCompanyDivisisIdResponse(params).pipe(
            __map(_r => _r.body as { success?: boolean; data?: Divisi; message?: string }),
        )
    }

    /**
     * Delete Divisi
     * @param id id of Divisi
     * @return successful operation
     */
    deleteCompanyDivisisIdResponse(
        id: string,
    ): __Observable<__StrictHttpResponse<{ success?: boolean; data?: string; message?: string }>> {
        let __params = this.newParams()
        let __headers = new HttpHeaders()
        let __body: any = null

        let req = new HttpRequest<any>('DELETE', this.rootUrl + `/company/divisis/${id}`, __body, {
            headers: __headers,
            params: __params,
            responseType: 'json',
        })

        return this.http.request<any>(req).pipe(
            __filter(_r => _r instanceof HttpResponse),
            __map(_r => {
                return _r as __StrictHttpResponse<{
                    success?: boolean
                    data?: string
                    message?: string
                }>
            }),
        )
    }
    /**
     * Delete Divisi
     * @param id id of Divisi
     * @return successful operation
     */
    deleteCompanyDivisisId(
        id: string,
    ): __Observable<{ success?: boolean; data?: string; message?: string }> {
        return this.deleteCompanyDivisisIdResponse(id).pipe(
            __map(_r => _r.body as { success?: boolean; data?: string; message?: string }),
        )
    }
}

module CompanyDivisiService {
    /**
     * Parameters for putCompanyDivisisId
     */
    export interface PutCompanyDivisisIdParams {
        /**
         * id of Divisi
         */
        id: string

        /**
         * Divisi that should be updated
         */
        body?: Divisi
    }
}

export { CompanyDivisiService }
