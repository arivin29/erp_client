/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Dokumen } from '../models/dokumen';
@Injectable({
  providedIn: 'root',
})
class FileDokumenService extends __BaseService {
  static readonly getFileDokumensPath = '/file/dokumens';
  static readonly postFileDokumensPath = '/file/dokumens';
  static readonly getFileDokumensIdPath = '/file/dokumens/{id}';
  static readonly putFileDokumensIdPath = '/file/dokumens/{id}';
  static readonly deleteFileDokumensIdPath = '/file/dokumens/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get all dokumens
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getFileDokumensResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Dokumen>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/file/dokumens`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Dokumen>, message?: string}>;
      })
    );
  }
  /**
   * Get all dokumens
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getFileDokumens(filter?: string): __Observable<{success?: boolean, data?: Array<Dokumen>, message?: string}> {
    return this.getFileDokumensResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Dokumen>, message?: string})
    );
  }

  /**
   * Store dokumen
   * @param body dokumen that should be stored
   * @return successful operation
   */
  postFileDokumensResponse(body?: Dokumen): __Observable<__StrictHttpResponse<{success?: boolean, data?: Dokumen, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/file/dokumens`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Dokumen, message?: string}>;
      })
    );
  }
  /**
   * Store dokumen
   * @param body dokumen that should be stored
   * @return successful operation
   */
  postFileDokumens(body?: Dokumen): __Observable<{success?: boolean, data?: Dokumen, message?: string}> {
    return this.postFileDokumensResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Dokumen, message?: string})
    );
  }

  /**
   * Get dokumen
   * @param id id of dokumen
   * @return successful operation
   */
  getFileDokumensIdResponse(id: number): __Observable<__StrictHttpResponse<{success?: boolean, data?: Dokumen, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/file/dokumens/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Dokumen, message?: string}>;
      })
    );
  }
  /**
   * Get dokumen
   * @param id id of dokumen
   * @return successful operation
   */
  getFileDokumensId(id: number): __Observable<{success?: boolean, data?: Dokumen, message?: string}> {
    return this.getFileDokumensIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Dokumen, message?: string})
    );
  }

  /**
   * Update dokumen
   * @param params The `FileDokumenService.PutFileDokumensIdParams` containing the following parameters:
   *
   * - `id`: id of dokumen
   *
   * - `body`: dokumen that should be updated
   *
   * @return successful operation
   */
  putFileDokumensIdResponse(params: FileDokumenService.PutFileDokumensIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Dokumen, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/file/dokumens/${params.id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Dokumen, message?: string}>;
      })
    );
  }
  /**
   * Update dokumen
   * @param params The `FileDokumenService.PutFileDokumensIdParams` containing the following parameters:
   *
   * - `id`: id of dokumen
   *
   * - `body`: dokumen that should be updated
   *
   * @return successful operation
   */
  putFileDokumensId(params: FileDokumenService.PutFileDokumensIdParams): __Observable<{success?: boolean, data?: Dokumen, message?: string}> {
    return this.putFileDokumensIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Dokumen, message?: string})
    );
  }

  /**
   * Delete dokumen
   * @param id id of dokumen
   * @return successful operation
   */
  deleteFileDokumensIdResponse(id: number): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/file/dokumens/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Delete dokumen
   * @param id id of dokumen
   * @return successful operation
   */
  deleteFileDokumensId(id: number): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteFileDokumensIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module FileDokumenService {

  /**
   * Parameters for putFileDokumensId
   */
  export interface PutFileDokumensIdParams {

    /**
     * id of dokumen
     */
    id: number;

    /**
     * dokumen that should be updated
     */
    body?: Dokumen;
  }
}

export { FileDokumenService }
