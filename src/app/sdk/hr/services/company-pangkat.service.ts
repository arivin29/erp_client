/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Pangkat } from '../models/pangkat';
@Injectable({
  providedIn: 'root',
})
class CompanyPangkatService extends __BaseService {
  static readonly getCompanyPangkatsPath = '/company/pangkats';
  static readonly postCompanyPangkatsPath = '/company/pangkats';
  static readonly getCompanyPangkatsIdPath = '/company/pangkats/{id}';
  static readonly putCompanyPangkatsIdPath = '/company/pangkats/{id}';
  static readonly deleteCompanyPangkatsIdPath = '/company/pangkats/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get all Pangkats
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCompanyPangkatsResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Pangkat>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/company/pangkats`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Pangkat>, message?: string}>;
      })
    );
  }
  /**
   * Get all Pangkats
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCompanyPangkats(filter?: string): __Observable<{success?: boolean, data?: Array<Pangkat>, message?: string}> {
    return this.getCompanyPangkatsResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Pangkat>, message?: string})
    );
  }

  /**
   * Store Pangkat
   * @param body Pangkat that should be stored
   * @return successful operation
   */
  postCompanyPangkatsResponse(body?: Pangkat): __Observable<__StrictHttpResponse<{success?: boolean, data?: Pangkat, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/company/pangkats`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Pangkat, message?: string}>;
      })
    );
  }
  /**
   * Store Pangkat
   * @param body Pangkat that should be stored
   * @return successful operation
   */
  postCompanyPangkats(body?: Pangkat): __Observable<{success?: boolean, data?: Pangkat, message?: string}> {
    return this.postCompanyPangkatsResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Pangkat, message?: string})
    );
  }

  /**
   * Get Pangkat
   * @param id id of Pangkat
   * @return successful operation
   */
  getCompanyPangkatsIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Pangkat, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/company/pangkats/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Pangkat, message?: string}>;
      })
    );
  }
  /**
   * Get Pangkat
   * @param id id of Pangkat
   * @return successful operation
   */
  getCompanyPangkatsId(id: string): __Observable<{success?: boolean, data?: Pangkat, message?: string}> {
    return this.getCompanyPangkatsIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Pangkat, message?: string})
    );
  }

  /**
   * Update Pangkat
   * @param params The `CompanyPangkatService.PutCompanyPangkatsIdParams` containing the following parameters:
   *
   * - `id`: id of Pangkat
   *
   * - `body`: Pangkat that should be updated
   *
   * @return successful operation
   */
  putCompanyPangkatsIdResponse(params: CompanyPangkatService.PutCompanyPangkatsIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Pangkat, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/company/pangkats/${params.id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Pangkat, message?: string}>;
      })
    );
  }
  /**
   * Update Pangkat
   * @param params The `CompanyPangkatService.PutCompanyPangkatsIdParams` containing the following parameters:
   *
   * - `id`: id of Pangkat
   *
   * - `body`: Pangkat that should be updated
   *
   * @return successful operation
   */
  putCompanyPangkatsId(params: CompanyPangkatService.PutCompanyPangkatsIdParams): __Observable<{success?: boolean, data?: Pangkat, message?: string}> {
    return this.putCompanyPangkatsIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Pangkat, message?: string})
    );
  }

  /**
   * Delete Pangkat
   * @param id id of Pangkat
   * @return successful operation
   */
  deleteCompanyPangkatsIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/company/pangkats/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Delete Pangkat
   * @param id id of Pangkat
   * @return successful operation
   */
  deleteCompanyPangkatsId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCompanyPangkatsIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CompanyPangkatService {

  /**
   * Parameters for putCompanyPangkatsId
   */
  export interface PutCompanyPangkatsIdParams {

    /**
     * id of Pangkat
     */
    id: string;

    /**
     * Pangkat that should be updated
     */
    body?: Pangkat;
  }
}

export { CompanyPangkatService }
