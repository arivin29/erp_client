/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Perizinan } from '../models/perizinan';
@Injectable({
  providedIn: 'root',
})
class CompanyPerizinanService extends __BaseService {
  static readonly getCompanyPerizinansPath = '/company/perizinans';
  static readonly postCompanyPerizinansPath = '/company/perizinans';
  static readonly getCompanyPerizinansIdPath = '/company/perizinans/{id}';
  static readonly putCompanyPerizinansIdPath = '/company/perizinans/{id}';
  static readonly deleteCompanyPerizinansIdPath = '/company/perizinans/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get all Perizinans
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCompanyPerizinansResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Perizinan>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/company/perizinans`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Perizinan>, message?: string}>;
      })
    );
  }
  /**
   * Get all Perizinans
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCompanyPerizinans(filter?: string): __Observable<{success?: boolean, data?: Array<Perizinan>, message?: string}> {
    return this.getCompanyPerizinansResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Perizinan>, message?: string})
    );
  }

  /**
   * Store Perizinan
   * @param body Perizinan that should be stored
   * @return successful operation
   */
  postCompanyPerizinansResponse(body?: Perizinan): __Observable<__StrictHttpResponse<{success?: boolean, data?: Perizinan, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/company/perizinans`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Perizinan, message?: string}>;
      })
    );
  }
  /**
   * Store Perizinan
   * @param body Perizinan that should be stored
   * @return successful operation
   */
  postCompanyPerizinans(body?: Perizinan): __Observable<{success?: boolean, data?: Perizinan, message?: string}> {
    return this.postCompanyPerizinansResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Perizinan, message?: string})
    );
  }

  /**
   * Get Perizinan
   * @param id id of Perizinan
   * @return successful operation
   */
  getCompanyPerizinansIdResponse(id: number): __Observable<__StrictHttpResponse<{success?: boolean, data?: Perizinan, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/company/perizinans/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Perizinan, message?: string}>;
      })
    );
  }
  /**
   * Get Perizinan
   * @param id id of Perizinan
   * @return successful operation
   */
  getCompanyPerizinansId(id: number): __Observable<{success?: boolean, data?: Perizinan, message?: string}> {
    return this.getCompanyPerizinansIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Perizinan, message?: string})
    );
  }

  /**
   * Update Perizinan
   * @param params The `CompanyPerizinanService.PutCompanyPerizinansIdParams` containing the following parameters:
   *
   * - `id`: id of Perizinan
   *
   * - `body`: Perizinan that should be updated
   *
   * @return successful operation
   */
  putCompanyPerizinansIdResponse(params: CompanyPerizinanService.PutCompanyPerizinansIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Perizinan, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/company/perizinans/${params.id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Perizinan, message?: string}>;
      })
    );
  }
  /**
   * Update Perizinan
   * @param params The `CompanyPerizinanService.PutCompanyPerizinansIdParams` containing the following parameters:
   *
   * - `id`: id of Perizinan
   *
   * - `body`: Perizinan that should be updated
   *
   * @return successful operation
   */
  putCompanyPerizinansId(params: CompanyPerizinanService.PutCompanyPerizinansIdParams): __Observable<{success?: boolean, data?: Perizinan, message?: string}> {
    return this.putCompanyPerizinansIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Perizinan, message?: string})
    );
  }

  /**
   * Delete Perizinan
   * @param id id of Perizinan
   * @return successful operation
   */
  deleteCompanyPerizinansIdResponse(id: number): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/company/perizinans/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Delete Perizinan
   * @param id id of Perizinan
   * @return successful operation
   */
  deleteCompanyPerizinansId(id: number): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCompanyPerizinansIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CompanyPerizinanService {

  /**
   * Parameters for putCompanyPerizinansId
   */
  export interface PutCompanyPerizinansIdParams {

    /**
     * id of Perizinan
     */
    id: number;

    /**
     * Perizinan that should be updated
     */
    body?: Perizinan;
  }
}

export { CompanyPerizinanService }
