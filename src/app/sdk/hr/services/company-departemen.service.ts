/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Departemen } from '../models/departemen';
@Injectable({
  providedIn: 'root',
})
class CompanyDepartemenService extends __BaseService {
  static readonly getCompanyDepartemensPath = '/company/departemens';
  static readonly postCompanyDepartemensPath = '/company/departemens';
  static readonly getCompanyDepartemensIdPath = '/company/departemens/{id}';
  static readonly putCompanyDepartemensIdPath = '/company/departemens/{id}';
  static readonly deleteCompanyDepartemensIdPath = '/company/departemens/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get all Departemens
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCompanyDepartemensResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Departemen>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/company/departemens`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Departemen>, message?: string}>;
      })
    );
  }
  /**
   * Get all Departemens
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCompanyDepartemens(filter?: string): __Observable<{success?: boolean, data?: Array<Departemen>, message?: string}> {
    return this.getCompanyDepartemensResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Departemen>, message?: string})
    );
  }

  /**
   * Store Departemen
   * @param body Departemen that should be stored
   * @return successful operation
   */
  postCompanyDepartemensResponse(body?: Departemen): __Observable<__StrictHttpResponse<{success?: boolean, data?: Departemen, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/company/departemens`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Departemen, message?: string}>;
      })
    );
  }
  /**
   * Store Departemen
   * @param body Departemen that should be stored
   * @return successful operation
   */
  postCompanyDepartemens(body?: Departemen): __Observable<{success?: boolean, data?: Departemen, message?: string}> {
    return this.postCompanyDepartemensResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Departemen, message?: string})
    );
  }

  /**
   * Get Departemen
   * @param id id of Departemen
   * @return successful operation
   */
  getCompanyDepartemensIdResponse(id: number): __Observable<__StrictHttpResponse<{success?: boolean, data?: Departemen, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/company/departemens/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Departemen, message?: string}>;
      })
    );
  }
  /**
   * Get Departemen
   * @param id id of Departemen
   * @return successful operation
   */
  getCompanyDepartemensId(id: number): __Observable<{success?: boolean, data?: Departemen, message?: string}> {
    return this.getCompanyDepartemensIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Departemen, message?: string})
    );
  }

  /**
   * Update Departemen
   * @param params The `CompanyDepartemenService.PutCompanyDepartemensIdParams` containing the following parameters:
   *
   * - `id`: id of Departemen
   *
   * - `body`: Departemen that should be updated
   *
   * @return successful operation
   */
  putCompanyDepartemensIdResponse(params: CompanyDepartemenService.PutCompanyDepartemensIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Departemen, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/company/departemens/${params.id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Departemen, message?: string}>;
      })
    );
  }
  /**
   * Update Departemen
   * @param params The `CompanyDepartemenService.PutCompanyDepartemensIdParams` containing the following parameters:
   *
   * - `id`: id of Departemen
   *
   * - `body`: Departemen that should be updated
   *
   * @return successful operation
   */
  putCompanyDepartemensId(params: CompanyDepartemenService.PutCompanyDepartemensIdParams): __Observable<{success?: boolean, data?: Departemen, message?: string}> {
    return this.putCompanyDepartemensIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Departemen, message?: string})
    );
  }

  /**
   * Delete Departemen
   * @param id id of Departemen
   * @return successful operation
   */
  deleteCompanyDepartemensIdResponse(id: number): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/company/departemens/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Delete Departemen
   * @param id id of Departemen
   * @return successful operation
   */
  deleteCompanyDepartemensId(id: number): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCompanyDepartemensIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CompanyDepartemenService {

  /**
   * Parameters for putCompanyDepartemensId
   */
  export interface PutCompanyDepartemensIdParams {

    /**
     * id of Departemen
     */
    id: number;

    /**
     * Departemen that should be updated
     */
    body?: Departemen;
  }
}

export { CompanyDepartemenService }
