/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Lisensi } from '../models/lisensi';
@Injectable({
  providedIn: 'root',
})
class CompanyLisensiService extends __BaseService {
  static readonly getCompanyLisensisPath = '/company/lisensis';
  static readonly postCompanyLisensisPath = '/company/lisensis';
  static readonly getCompanyLisensisIdPath = '/company/lisensis/{id}';
  static readonly putCompanyLisensisIdPath = '/company/lisensis/{id}';
  static readonly deleteCompanyLisensisIdPath = '/company/lisensis/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get all Lisensis
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCompanyLisensisResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Lisensi>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/company/lisensis`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Lisensi>, message?: string}>;
      })
    );
  }
  /**
   * Get all Lisensis
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCompanyLisensis(filter?: string): __Observable<{success?: boolean, data?: Array<Lisensi>, message?: string}> {
    return this.getCompanyLisensisResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Lisensi>, message?: string})
    );
  }

  /**
   * Store Lisensi
   * @param body Lisensi that should be stored
   * @return successful operation
   */
  postCompanyLisensisResponse(body?: Lisensi): __Observable<__StrictHttpResponse<{success?: boolean, data?: Lisensi, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/company/lisensis`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Lisensi, message?: string}>;
      })
    );
  }
  /**
   * Store Lisensi
   * @param body Lisensi that should be stored
   * @return successful operation
   */
  postCompanyLisensis(body?: Lisensi): __Observable<{success?: boolean, data?: Lisensi, message?: string}> {
    return this.postCompanyLisensisResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Lisensi, message?: string})
    );
  }

  /**
   * Get Lisensi
   * @param id id of Lisensi
   * @return successful operation
   */
  getCompanyLisensisIdResponse(id: number): __Observable<__StrictHttpResponse<{success?: boolean, data?: Lisensi, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/company/lisensis/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Lisensi, message?: string}>;
      })
    );
  }
  /**
   * Get Lisensi
   * @param id id of Lisensi
   * @return successful operation
   */
  getCompanyLisensisId(id: number): __Observable<{success?: boolean, data?: Lisensi, message?: string}> {
    return this.getCompanyLisensisIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Lisensi, message?: string})
    );
  }

  /**
   * Update Lisensi
   * @param params The `CompanyLisensiService.PutCompanyLisensisIdParams` containing the following parameters:
   *
   * - `id`: id of Lisensi
   *
   * - `body`: Lisensi that should be updated
   *
   * @return successful operation
   */
  putCompanyLisensisIdResponse(params: CompanyLisensiService.PutCompanyLisensisIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Lisensi, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/company/lisensis/${params.id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Lisensi, message?: string}>;
      })
    );
  }
  /**
   * Update Lisensi
   * @param params The `CompanyLisensiService.PutCompanyLisensisIdParams` containing the following parameters:
   *
   * - `id`: id of Lisensi
   *
   * - `body`: Lisensi that should be updated
   *
   * @return successful operation
   */
  putCompanyLisensisId(params: CompanyLisensiService.PutCompanyLisensisIdParams): __Observable<{success?: boolean, data?: Lisensi, message?: string}> {
    return this.putCompanyLisensisIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Lisensi, message?: string})
    );
  }

  /**
   * Delete Lisensi
   * @param id id of Lisensi
   * @return successful operation
   */
  deleteCompanyLisensisIdResponse(id: number): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/company/lisensis/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Delete Lisensi
   * @param id id of Lisensi
   * @return successful operation
   */
  deleteCompanyLisensisId(id: number): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCompanyLisensisIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CompanyLisensiService {

  /**
   * Parameters for putCompanyLisensisId
   */
  export interface PutCompanyLisensisIdParams {

    /**
     * id of Lisensi
     */
    id: number;

    /**
     * Lisensi that should be updated
     */
    body?: Lisensi;
  }
}

export { CompanyLisensiService }
