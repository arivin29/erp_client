/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Acara } from '../models/acara';
@Injectable({
  providedIn: 'root',
})
class CompanyAcaraService extends __BaseService {
  static readonly getCompanyAcarasPath = '/company/acaras';
  static readonly postCompanyAcarasPath = '/company/acaras';
  static readonly getCompanyAcarasIdPath = '/company/acaras/{id}';
  static readonly putCompanyAcarasIdPath = '/company/acaras/{id}';
  static readonly deleteCompanyAcarasIdPath = '/company/acaras/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get all Acaras
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCompanyAcarasResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Acara>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/company/acaras`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Acara>, message?: string}>;
      })
    );
  }
  /**
   * Get all Acaras
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCompanyAcaras(filter?: string): __Observable<{success?: boolean, data?: Array<Acara>, message?: string}> {
    return this.getCompanyAcarasResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Acara>, message?: string})
    );
  }

  /**
   * Store Acara
   * @param body Acara that should be stored
   * @return successful operation
   */
  postCompanyAcarasResponse(body?: Acara): __Observable<__StrictHttpResponse<{success?: boolean, data?: Acara, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/company/acaras`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Acara, message?: string}>;
      })
    );
  }
  /**
   * Store Acara
   * @param body Acara that should be stored
   * @return successful operation
   */
  postCompanyAcaras(body?: Acara): __Observable<{success?: boolean, data?: Acara, message?: string}> {
    return this.postCompanyAcarasResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Acara, message?: string})
    );
  }

  /**
   * Get Acara
   * @param id id of Acara
   * @return successful operation
   */
  getCompanyAcarasIdResponse(id: number): __Observable<__StrictHttpResponse<{success?: boolean, data?: Acara, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/company/acaras/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Acara, message?: string}>;
      })
    );
  }
  /**
   * Get Acara
   * @param id id of Acara
   * @return successful operation
   */
  getCompanyAcarasId(id: number): __Observable<{success?: boolean, data?: Acara, message?: string}> {
    return this.getCompanyAcarasIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Acara, message?: string})
    );
  }

  /**
   * Update Acara
   * @param params The `CompanyAcaraService.PutCompanyAcarasIdParams` containing the following parameters:
   *
   * - `id`: id of Acara
   *
   * - `body`: Acara that should be updated
   *
   * @return successful operation
   */
  putCompanyAcarasIdResponse(params: CompanyAcaraService.PutCompanyAcarasIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Acara, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/company/acaras/${params.id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Acara, message?: string}>;
      })
    );
  }
  /**
   * Update Acara
   * @param params The `CompanyAcaraService.PutCompanyAcarasIdParams` containing the following parameters:
   *
   * - `id`: id of Acara
   *
   * - `body`: Acara that should be updated
   *
   * @return successful operation
   */
  putCompanyAcarasId(params: CompanyAcaraService.PutCompanyAcarasIdParams): __Observable<{success?: boolean, data?: Acara, message?: string}> {
    return this.putCompanyAcarasIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Acara, message?: string})
    );
  }

  /**
   * Delete Acara
   * @param id id of Acara
   * @return successful operation
   */
  deleteCompanyAcarasIdResponse(id: number): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/company/acaras/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Delete Acara
   * @param id id of Acara
   * @return successful operation
   */
  deleteCompanyAcarasId(id: number): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCompanyAcarasIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CompanyAcaraService {

  /**
   * Parameters for putCompanyAcarasId
   */
  export interface PutCompanyAcarasIdParams {

    /**
     * id of Acara
     */
    id: number;

    /**
     * Acara that should be updated
     */
    body?: Acara;
  }
}

export { CompanyAcaraService }
