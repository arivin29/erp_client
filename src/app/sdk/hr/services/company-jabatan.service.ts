/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { V_jabatan } from '../models/v_jabatan';
import { Jabatan } from '../models/jabatan';
@Injectable({
  providedIn: 'root',
})
class CompanyJabatanService extends __BaseService {
  static readonly getCompanyJabatansPath = '/company/jabatans';
  static readonly postCompanyJabatansPath = '/company/jabatans';
  static readonly getCompanyJabatansIdPath = '/company/jabatans/{id}';
  static readonly putCompanyJabatansIdPath = '/company/jabatans/{id}';
  static readonly deleteCompanyJabatansIdPath = '/company/jabatans/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get all Jabatans
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCompanyJabatansResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<V_jabatan>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/company/jabatans`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<V_jabatan>, message?: string}>;
      })
    );
  }
  /**
   * Get all Jabatans
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCompanyJabatans(filter?: string): __Observable<{success?: boolean, data?: Array<V_jabatan>, message?: string}> {
    return this.getCompanyJabatansResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<V_jabatan>, message?: string})
    );
  }

  /**
   * Store Jabatan
   * @param body Jabatan that should be stored
   * @return successful operation
   */
  postCompanyJabatansResponse(body?: Jabatan): __Observable<__StrictHttpResponse<{success?: boolean, data?: Jabatan, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/company/jabatans`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Jabatan, message?: string}>;
      })
    );
  }
  /**
   * Store Jabatan
   * @param body Jabatan that should be stored
   * @return successful operation
   */
  postCompanyJabatans(body?: Jabatan): __Observable<{success?: boolean, data?: Jabatan, message?: string}> {
    return this.postCompanyJabatansResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Jabatan, message?: string})
    );
  }

  /**
   * Get Jabatan
   * @param id id of Jabatan
   * @return successful operation
   */
  getCompanyJabatansIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Jabatan, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/company/jabatans/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Jabatan, message?: string}>;
      })
    );
  }
  /**
   * Get Jabatan
   * @param id id of Jabatan
   * @return successful operation
   */
  getCompanyJabatansId(id: string): __Observable<{success?: boolean, data?: Jabatan, message?: string}> {
    return this.getCompanyJabatansIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Jabatan, message?: string})
    );
  }

  /**
   * Update Jabatan
   * @param params The `CompanyJabatanService.PutCompanyJabatansIdParams` containing the following parameters:
   *
   * - `id`: id of Jabatan
   *
   * - `body`: Jabatan that should be updated
   *
   * @return successful operation
   */
  putCompanyJabatansIdResponse(params: CompanyJabatanService.PutCompanyJabatansIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Jabatan, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/company/jabatans/${params.id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Jabatan, message?: string}>;
      })
    );
  }
  /**
   * Update Jabatan
   * @param params The `CompanyJabatanService.PutCompanyJabatansIdParams` containing the following parameters:
   *
   * - `id`: id of Jabatan
   *
   * - `body`: Jabatan that should be updated
   *
   * @return successful operation
   */
  putCompanyJabatansId(params: CompanyJabatanService.PutCompanyJabatansIdParams): __Observable<{success?: boolean, data?: Jabatan, message?: string}> {
    return this.putCompanyJabatansIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Jabatan, message?: string})
    );
  }

  /**
   * Delete Jabatan
   * @param id id of Jabatan
   * @return successful operation
   */
  deleteCompanyJabatansIdResponse(id: number): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/company/jabatans/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Delete Jabatan
   * @param id id of Jabatan
   * @return successful operation
   */
  deleteCompanyJabatansId(id: number): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCompanyJabatansIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CompanyJabatanService {

  /**
   * Parameters for putCompanyJabatansId
   */
  export interface PutCompanyJabatansIdParams {

    /**
     * id of Jabatan
     */
    id: string;

    /**
     * Jabatan that should be updated
     */
    body?: Jabatan;
  }
}

export { CompanyJabatanService }
