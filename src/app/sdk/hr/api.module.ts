/* tslint:disable */
import { NgModule, ModuleWithProviders } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ApiConfiguration, ApiConfigurationInterface } from './api-configuration';

import { CompanyAcaraService } from './services/company-acara.service';
import { CompanyDepartemenService } from './services/company-departemen.service';
import { CompanyDivisiService } from './services/company-divisi.service';
import { CompanyJabatanService } from './services/company-jabatan.service';
import { CompanyKantorService } from './services/company-kantor.service';
import { CompanyLisensiService } from './services/company-lisensi.service';
import { CompanyPangkatService } from './services/company-pangkat.service';
import { CompanyPengumumanService } from './services/company-pengumuman.service';
import { CompanyPerizinanService } from './services/company-perizinan.service';
import { CompanyPerusahaanService } from './services/company-perusahaan.service';
import { FileDokumenService } from './services/file-dokumen.service';

/**
 * Provider for all Api services, plus ApiConfiguration
 */
@NgModule({
  imports: [
    HttpClientModule
  ],
  exports: [
    HttpClientModule
  ],
  declarations: [],
  providers: [
    ApiConfiguration,
    CompanyAcaraService,
    CompanyDepartemenService,
    CompanyDivisiService,
    CompanyJabatanService,
    CompanyKantorService,
    CompanyLisensiService,
    CompanyPangkatService,
    CompanyPengumumanService,
    CompanyPerizinanService,
    CompanyPerusahaanService,
    FileDokumenService
  ],
})
export class ApiModule {
  static forRoot(customParams: ApiConfigurationInterface): ModuleWithProviders {
    return {
      ngModule: ApiModule,
      providers: [
        {
          provide: ApiConfiguration,
          useValue: {rootUrl: customParams.rootUrl}
        }
      ]
    }
  }
}
