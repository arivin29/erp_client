import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { PurchasingComponent } from './purchasing.component'

const routes: Routes = [
    {
        path: '',
        component: PurchasingComponent,
        children: [],
    },
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PurchasingRoutingModule {}
