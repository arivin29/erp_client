import { Component, OnInit } from '@angular/core'
import * as Reducers from 'src/app/store/reducers'
import {
    slideFadeinUp,
    slideFadeinRight,
    zoomFadein,
    fadein,
} from 'src/app/layouts/App/router-animations'
import { Observable } from 'rxjs'
import { Store, select } from '@ngrx/store'
import { MenuService } from 'src/app/services/menu.service'
import { RouterOutlet } from '@angular/router'

@Component({
    selector: 'app-purchasing',
    templateUrl: './purchasing.component.html',
    styleUrls: ['./purchasing.component.scss'],
    animations: [slideFadeinUp, slideFadeinRight, zoomFadein, fadein],
})
export class PurchasingComponent implements OnInit {
    settings$: Observable<any>
    menuLayoutType: string
    isContentNoMaxWidth: boolean
    isAppMaxWidth: boolean
    isGrayBackground: boolean
    isSquaredBorders: boolean
    isCardShadow: boolean
    isBorderless: boolean
    isTopbarFixed: boolean
    isGrayTopbar: boolean
    routerAnimation: string

    constructor(private store: Store<any>, private menuService: MenuService) {
        this.store.pipe(select(Reducers.getSettings)).subscribe(state => {
            this.menuLayoutType = state.menuLayoutType
            this.isContentNoMaxWidth = state.isContentNoMaxWidth
            this.isAppMaxWidth = state.isAppMaxWidth
            this.isGrayBackground = state.isGrayBackground
            this.isSquaredBorders = state.isSquaredBorders
            this.isCardShadow = state.isCardShadow
            this.isBorderless = state.isBorderless
            this.isTopbarFixed = state.isTopbarFixed
            this.isGrayTopbar = state.isGrayTopbar
            this.routerAnimation = state.routerAnimation
        })
    }

    menuData: any = []
    ngOnInit() {
        this.menuData = [
            {
                title: 'Maintenance',
                key: 'Maintenance',
                icon: 'fe fe-home',
                count: 6,
                children: [
                    {
                        title: 'Repair',
                        key: 'maintenance/repair',
                        icon: 'fe fe-bookmark',
                        url: 'maintenance/repair',
                    },
                    {
                        title: 'Archived',
                        key: 'maintenance/archived',
                        icon: 'fe fe-bookmark',
                        url: 'maintenance/archived',
                    },
                ],
            },
            {
                title: 'Transaksi',
                key: 'masuk',
                icon: 'fe fe-home',
                count: 6,
                children: [
                    {
                        title: 'Masuk',
                        key: 'transaction_checkin',
                        icon: 'fe fe-bookmark',
                        url: 'transaction/checkin',
                    },

                    {
                        title: 'Keluar',
                        key: 'transaction_checkout',
                        icon: 'fe fe-bookmark',
                        url: 'transaction/checkout',
                    },
                ],
            },

            {
                title: 'Assets',
                key: 'Assets',
                icon: 'fe fe-bookmark',
                url: 'list/asset',
            },

            {
                title: 'Component',
                key: 'Component',
                icon: 'fe fe-bookmark',
                url: 'list/componen',
            },

            {
                title: 'Licensi',
                key: 'Licensi',
                icon: 'fe fe-bookmark',
                url: 'list/license',
            },

            {
                title: 'Consumable',
                key: 'Consumable',
                icon: 'fe fe-bookmark',
                url: 'list/consumable',
            },

            {
                title: 'Library',
                key: 'master',
                icon: 'fe fe-home',
                count: 6,
                children: [
                    {
                        title: 'Status',
                        key: 'master/status',
                        icon: 'fe fe-bookmark',
                        url: '../asset/master/status',
                    },
                    {
                        title: 'Model',
                        key: 'model',
                        icon: 'fe fe-bookmark',
                        url: '../asset/master/model',
                    },
                    {
                        title: 'Maintenance Code',
                        key: 'maintenance-code',
                        icon: 'fe fe-bookmark',
                        url: '../asset/master/maintenance-code',
                    },
                    {
                        title: 'Owner',
                        key: 'owner',
                        icon: 'fe fe-bookmark',
                        url: '../asset/master/owner',
                    },
                    {
                        title: 'category',
                        key: 'owner',
                        icon: 'fe fe-bookmark',
                        url: '../asset/master/category',
                    },
                ],
            },

            {
                title: 'Supplier',
                key: 'suplier',
                icon: 'fe fe-home',
                count: 6,
                children: [
                    {
                        title: 'Contact',
                        key: 'supplier/contact',
                        icon: 'fe fe-bookmark',
                        url: '../asset/supplier/contact',
                    },
                    {
                        title: 'Manufacture',
                        key: 'supplier/manufacture',
                        icon: 'fe fe-bookmark',
                        url: '../asset/supplier/manufacture',
                    },
                    {
                        title: 'Vendor',
                        key: 'vendor',
                        icon: 'fe fe-bookmark',
                        url: '../asset/supplier/vendor',
                    },
                ],
            },
        ]

        this.menuService.menu.next(this.menuData)
    }

    routeAnimation(outlet: RouterOutlet, animation: string) {
        if (animation === this.routerAnimation) {
            return outlet.isActivated && outlet.activatedRoute.routeConfig.path
        }
    }
}
