import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { PurchasingRoutingModule } from './purchasing-routing.module'
import { PurchasingComponent } from './purchasing.component'
import { ApiModule } from 'src/app/sdk/hr/api.module'
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar'
import { WidgetsComponentsModule } from 'src/app/components/widgets/widgets-components.module'
import { NzLayoutModule, NgZorroAntdModule } from 'ng-zorro-antd'
import { LayoutModule } from 'src/app/components/layout/layout.module'
import { FormsModule } from '@angular/forms'
import { FormBuilderTypeSafe } from 'src/app/services/angular-reactive-forms-helper'
import { ApiModule as ApiModuleHr } from 'src/app/sdk/hr/api.module'

@NgModule({
    declarations: [PurchasingComponent],
    imports: [
        CommonModule,
        PurchasingRoutingModule,
        ApiModule.forRoot({ rootUrl: 'http://erp-asset.devetek.com/api' }),
        ApiModuleHr.forRoot({ rootUrl: 'http://dev.devetek.com/api' }),
        PerfectScrollbarModule,
        WidgetsComponentsModule,
        NzLayoutModule,
        LayoutModule,
        NgZorroAntdModule,
        FormsModule,
    ],
    providers: [FormBuilderTypeSafe],
})
export class PurchasingModule {}
